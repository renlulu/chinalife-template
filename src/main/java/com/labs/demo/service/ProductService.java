package com.labs.demo.service;

import com.labs.demo.controller.converter.ProductConverter;
import com.labs.demo.controller.vo.ProductVo;
import com.labs.demo.domain.Product;
import com.labs.demo.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ProductService {
    private ProductRepository productRepository;
    private ProductConverter productConverter;

    @Autowired
    public ProductService(ProductRepository productRepository,
                          ProductConverter productConverter) {
        this.productRepository = productRepository;
        this.productConverter = productConverter;
    }

    public ProductVo save(ProductVo productVo) {
        Product product =
                productRepository.save(productConverter.reverse().convert(productVo));
        return productConverter.convert(product);
    }

    public List<ProductVo> all() {
        Iterable<Product> products = productRepository.findAll();
        return StreamSupport.stream(products.spliterator(), false)
                .map(productConverter::convert)
                .collect(Collectors.toList());

    }
}
