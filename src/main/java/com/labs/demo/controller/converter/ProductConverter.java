package com.labs.demo.controller.converter;

import com.google.common.base.Converter;
import com.labs.demo.controller.vo.ProductVo;
import com.labs.demo.domain.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductConverter extends Converter<Product, ProductVo> {

    @Override
    protected ProductVo doForward(Product product) {
        return ProductVo.builder()
                .id(product.getId())
                .name(product.getName())
                .prize(product.getPrize())
                .build();
    }

    @Override
    protected Product doBackward(ProductVo productVo) {
        return Product.builder()
                .name(productVo.getName())
                .prize(productVo.getPrize())
                .build();
    }
}
